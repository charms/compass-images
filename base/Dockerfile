ARG BASE_IMAGE
FROM ${BASE_IMAGE}

SHELL ["/bin/bash", "-c"]

# The following two lines are necessary for the configuration of tzdata
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# We need python3-dev to have dynamic libraries at runtime
RUN apt-get update && apt-get install --yes --no-install-recommends \
    python3-full \
    python3-pip \
    libpython3-dev \
    python-is-python3 \
    wget \
    curl \
    build-essential \
    ninja-build \
    git \
    clang-format \
    gcc \
    gfortran \
    cmake \
    libboost-dev \
    libeigen3-dev \
    libmpfr-dev \
    libopenmpi-dev \
    liblapack-dev \
    libmetis-dev \
#    libpetsc-real-dev \
    vim  \
    cmake-curses-gui \
#    ipython3 \
    pandoc \
    texlive \
    texlive-pictures \
    texlive-latex-extra \
    doxygen \
    graphviz \
    pdf2svg \
 && apt-get clean

# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
ENV VIRTUAL_ENV=/compass
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install \
    docutils \
    pyyaml \
    sortedcontainers \
    pre-commit \
    pathspec \
    pyproject-metadata \
    build \
    scikit-build-core \
    pybind11 \
    setuptools \
    setuptools_scm \
    setuptools_git_versioning \
    sphinx \
    sphinx-rtd-theme \
    sphinxcontrib-tikz \
    sphinx-revealjs \
    recommonmark \
    breathe \
    psutil \
    inept==0.1.0 \
    verstr \
    click \
    numpy \
    setuptools \
    pytest-forked \
    pytest-xdist \
    twine \
    wheel \
    matplotlib \
    numba \
    scipy \
    cython \
    meshio

VOLUME /localfs
WORKDIR /localfs
