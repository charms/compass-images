Project dedicated to store ComPASS dependencies and build ComPASS docker images.

To add ComPASS dependencies as 3rd party python packages to the project registry use twine:

```bash
twine upload --repository-url https://gitlab.inria.fr/api/v4/projects/30678/packages/pypi dist/*
```

where the `dist` directory contains the wheels that you want to upload.

You will need to be authenticated on `gitlab.inria.fr` using username and password/token.
